using Microsoft.AspNetCore.Mvc;

namespace Exercice2_MVC.Controllers
{
    public class Home : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Download()
        {
            return File("~/Documents/420B0-2017-A18.pdf", "application/pdf");
        }
        
        public IActionResult GotoCegep()
        {
            return Redirect("https://www.cstjean.qc.ca/");
        }

    }
}