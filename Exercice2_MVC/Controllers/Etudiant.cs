using Microsoft.AspNetCore.Mvc;

namespace Exercice2_MVC.Controllers
{
    public class Etudiant : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            var philemon = new Models.Etudiant
            {
                Nom = "Philemon",
                Age = 19,
                Courriel = "1729860@cstjean.qc.ca",
                Matricule = "inconnu",
                Programme = "Info",
                Telephone = "secret"
            };
            return View("Index", philemon);
        }
    }
}