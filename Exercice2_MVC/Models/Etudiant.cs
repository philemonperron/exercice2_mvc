namespace Exercice2_MVC.Models
{
    public class Etudiant
    {
        public string Nom { get; set; }
        public string Programme { get; set; }
        public int Age { get; set; }
        public string Matricule { get; set; }
        public string Telephone { get; set; }
        public string Courriel { get; set; }
    }
}